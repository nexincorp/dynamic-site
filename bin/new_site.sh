#!/bin/bash

#echo $1
site=$1
sandbox_site=$1.sandbox
#echo $sandbox_site
site_underscore="${site//./_}"
#echo $sandbox_site_underscore

#add domain to hosts
echo "Adding domain to hosts..."
echo "127.0.0.1 ${sandbox_site}" >> /etc/hosts

#echo $sandbox_site_underscore
echo "Creating database..."
docker exec -it mariadb_container mysql -u root -ptoor -e "CREATE DATABASE $site_underscore"

echo "Creating default tables and contents..."
docker exec -it nexin_php_container bash -c "cd /var/www/nexin/core && php artisan migrate --database=${site_underscore}"

echo "Creating default configurations (ymls, migrations, FE js, styles and pages) on /site_configurations..."
cp -r sites_configurations/samplesite_sandbox sites_configurations/${site_underscore}
sudo_user_group=$(id -g -n $SUDO_USER)
chown $SUDO_USER:$sudo_user_group -R sites_configurations/${site_underscore}

#php artisan migrate --database=${sandbox_site_underscore} --path="/sites_configurations/${sandbox_site_underscore}/user/database/migrations"
