<?php

namespace Osds\DynamicSite\Laravel\Traits;


trait StaticPagesTrait {

    var $static_pages = [];
    var $static_pages_raw = [];

    private function getStaticPages()
    {
        $filter = [];
        $static_pages_raw = $this->apiRequest('static_pages', 'list', $filter);
        $static_pages = $this->preTreatDataBeforeDisplaying('static_pages', $static_pages_raw, true);
        foreach($static_pages['items'] as $key => $static_page)
        {
            $static_page['multilang_seo_names'] = json_decode($static_pages_raw['items'][$key]['seo_name'], true);
            $this->static_pages[$static_page['seo_name']] = $static_page;
        }
    }

}