<?php

namespace Osds\DynamicSite\Traits;

use Symfony\Component\Yaml\Yaml;

trait UtilsTrait
{

    public function loadSiteConfiguration()
    {
        $this->config['site']['id'] = get_server_id();
        $this->config['site']['path'] = base_path('/sites_configurations/' . $this->config['site']['id'] . '/');
        $this->loadConfigFile('domain_structure');
        $this->loadConfigFile('public');
    }

    public function getAlertMessages()
    {
        $message = null;

        if(isset($this->request_data['get']['action_message']))
        {
            $message = ['message' => $this->request_data['get']['action_message'] ];
            if(isset($this->request_data['get']['action_result']))
            {
                $message['type'] = $this->request_data['get']['action_result'];
            } else {
                $message['type'] = 'info';
            }
        }

        return $message;
    }

    public function redirect($url, $result = null, $message = null, $error = null)
    {
        $locale = $this->loadLocalization($this->vendor_path . 'Localization/');

        $url = '/' . BACKOFFICE_FOLDER . $url;
        if($message != null)
        {
            if(isset($locale[strtoupper($message)]))
            {
                $message = $locale[strtoupper($message)];
            }
            $url .= '?action_message=' . $message;

            if($error != null) {
                if(is_string($error)) {
                    $url .= $error;
                } else {
                    $url .= $error->getMessage() . ' @ ' . basename($error->getFile()) . '::' . $error->getLine();
                }
            }

            if($result != null)
            {
                $url .= '&action_result=' . $result;
            }
        }
        header('Location: ' . $url);
        exit;
    }


    /**
     * Load Configuration File
     */
    private function loadConfigFile($type)
    {
        $file = $this->config['site']['path'] . "user/config/{$type}.yml";
        if(!is_file($file))
        {
            return false;
        }
        $this->config[$type] = Yaml::parse(file_get_contents($file));
    }

    public function folderSize($path) {
        $total_size = 0;
        $files = scandir($path);
        $cleanPath = rtrim($path, '/') . '/';

        foreach ($files as $t) {
            if ($t <> "." && $t <> "..") {
                $currentFile = $cleanPath . $t;
                if (is_dir($currentFile)) {
                    $size = $this->foldersize($currentFile);
                    $total_size += $size;
                } else {
                    $size = filesize($currentFile);
                    $total_size += $size;
                }
            }
        }
        return $total_size;
    }

    private function isMultilanguageField($field)
    {
        return
            is_array($field)
            && array_keys($field) == $this->config['public']['languages']
            ;
    }

    private function getVisitorLanguage()
    {
        if(
            isset($_GET['lang'])
            && in_array($_GET['lang'], $this->config['public']['languages'])
        )
        {
            $lang_to_use = $_GET['lang'];
        } else {
            $lang_to_use = $this->config['public']['language'];
        }
        $this->session->put('visitor_language', $lang_to_use);
        $this->visitor_language = $this->session->get('visitor_language');
    }

    private function preTreatDataBeforeDisplaying($model, $data, $localize = false)
    {

        if(@count($data['items']) > 0)
        {

            #treat multilanguage fields
            if(
                isset($this->config['public']['languages'])
                && isset($this->config['domain_structure']['models'][$model]['schema']['multilanguage_fields'])
            ) {
                foreach($data['items'] as &$item)
                {
                    foreach($this->config['domain_structure']['models'][$model]['schema']['multilanguage_fields'] as $ml_field)
                    {
                        $decoded_ml_field = json_decode($item[$ml_field], true);
                        // check if it really is a multilanguage field
                        if(json_last_error() != JSON_ERROR_NONE
                        || !isset($decoded_ml_field[$this->visitor_language])) {
                            continue;
                        } else {
                            $item[$ml_field] = $decoded_ml_field;
                        }

                        #preserve only a desired language
                        if(
                            $localize
                            && is_array($item[$ml_field])
                        )
                        {
                            #check if we have at least one item of the array that is a valid language
                            if(
                                isset($this->visitor_language)
                                && count(array_intersect(array_keys($item[$ml_field]), $this->config['public']['languages'])) > 0
                                && in_array($this->visitor_language, array_keys($item[$ml_field]))
                            ) {
                                #visitor language has a defined value on the field array
                                $item[$ml_field] = $item[$ml_field][$this->visitor_language];
                            } else {
                                #user language is not defined, use first
                                $item[$ml_field] = current($item[$ml_field]);
                            }
                        } else {
//                            $item[$ml_field] = $item;
                        }
                    }
                }
            }
        }

        return $data;
    }

    public static function checkSeoName($value)
    {
        $from = array(
            'á','À','Á','Â','Ã','Ä','Å',
            'ß','Ç',
            'é','è','ë','È','É','Ê','Ë',
            'í','ì','ï','Ì','Í','Î','Ï','Ñ',
            'ó','ò','ö','Ò','Ó','Ô','Õ','Ö',
            'ú','ù','ü','Ù','Ú','Û','Ü');

        $to = array(
            'a','A','A','A','A','A','A',
            'B','C',
            'e','e','e','E','E','E','E',
            'i','i','i','I','I','I','I','N',
            'o','o','o','O','O','O','O','O',
            'u','u','u','U','U','U','U');

        $value = str_replace($from, $to, $value);
        $value = str_replace(' ', '-', $value);
        $value = strtolower($value);
        $words = preg_split("#[^a-z0-9]#", $value, -1, PREG_SPLIT_NO_EMPTY);
        return implode("-", $words);
    }

}