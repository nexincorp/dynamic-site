<?php

namespace Osds\DynamicSite\Laravel\Traits;

use Illuminate\Support\Facades\Session;
//use Illuminate\View\View;
//use TwigBridge\Extension\Laravel\Html;

use Leafo\ScssPhp\Compiler as ScssCompiler;
use Osds\Backoffice\Application\Traits\CallbacksTrait;
use Osds\DynamicSite\Traits\UtilsTrait;

trait ViewTrait {

    public $twig_vars;
    public $view_path;

    private $theme_common_path = __DIR__ . '/../../themes/common/';
    private $theme_site_path = '/sites_configurations/%s/user/layout/';

    protected function generateView($view_path = null)
    {
        view()->addLocation($this->theme_common_path . 'templates');

        $this->loadBaseTwigVars();

        if($view_path == null)
        {
            $view_path = $this->view_path;
        }

        return View($view_path, $this->twig_vars);
    }

    private function loadBaseTwigVars()
    {
        $this->twig_vars['alert_message'] = $this->session->get('alert_message');
        $this->session->remove('alert_message');

        $this->loadConfigVars();

        $this->getViewPath();
        $this->getViewVars();

        $this->loadCssJs();

        $this->generateNavbar($this->request_uri[0]);
        $this->handlePageContents();

        $this->getLanguages();
    }

    private function loadConfigVars()
    {
        $this->twig_vars['config']['env'] = env('APP_ENV');
        $this->twig_vars['config'] = $this->config['public'];
        $this->twig_vars['config']['site'] = $this->config['site'];

    }

    private function getViewVars()
    {
        $this->twig_vars['page_id'] =
            isset($_SERVER['REDIRECT_URL'])?
                UtilsTrait::checkSeoName($_SERVER['REDIRECT_URL'])
                :'index';

        $this->twig_vars['site_domain'] = $_SERVER['HTTP_HOST'];

        if($this->content_type == 'dyco')
        {
            $this->twig_vars['model_name'] = $this->request_uri[0];
            $this->twig_vars['model_schema'] = $this->config['domain_structure']['models'][$this->request_uri[0]]['schema'];
        }
    }

    private function getViewPath()
    {
        if(!isset($this->content_type))
        {
            return null;
        }

        if($this->content_type == 'static')
        {
            $this->view_path = 'pages/static_page';

        } else if($this->content_type == 'dyco')
        {
            $this->view_path = 'pages/dyco/' . $this->action_type;

        } else if($this->content_type == 'contact')
        {
            $custom_gdpr_path = base_path(sprintf($this->theme_site_path, $this->config['site']['id']) . '/pages/contact.twig');
            $this->twig_vars['contact_page_custom_data'] = @file_get_contents($custom_gdpr_path);
            $this->view_path = 'pages/contact';

        } else if($this->content_type == 'samples')
        {
            $this->view_path = 'pages/samples';

        } else if($this->content_type == 'gdpr')
        {
            $custom_gdpr_path = base_path(sprintf($this->theme_site_path, $this->config['site']['id']) . '/pages/gdpr.twig');
            $this->twig_vars['custom_gdpr'] = @file_get_contents($custom_gdpr_path);
            $this->view_path = 'pages/gdpr';
        }
    }


    private function loadCssJs()
    {
        $theme_site_path = base_path(sprintf($this->theme_site_path, $this->config['site']['id']));

        $types = ['styles' => 'css', 'javascript' => 'js'];

        foreach($types as $type => $ext)
        {
            $file_destiny = public_path() . "/{$type}/{$this->config['site']['id']}.{$ext}";
            if(
                file_exists($file_destiny)
                && !isset($_GET['clear_cache'])
            ) continue;

            $file_contents = '';

            $files = array_merge(
                glob($this->theme_common_path . $type . '/*.' . $ext),
                glob($theme_site_path . $type . '/*.' . $ext)
            );

            foreach($files as $file)
            {
                $file_contents .= file_get_contents($file);
            }

            if($type == 'styles')
            {
                #treat scss
                $scss_to_compile = '';

                #custom scss variables for this site
                $scss_custom_file_config = $theme_site_path . "styles/main.scss";
                $scss_to_compile .= @file_get_contents($scss_custom_file_config);

                #custom scss variables for the common parts
                $scss_custom_common_files = glob($theme_site_path . "styles/common/*.scss");
                foreach($scss_custom_common_files as $scss_custom_common_file) {
                    $scss_to_compile .= file_get_contents($scss_custom_common_file);
                }

                #all common scss variables
                $scss_common_files = glob($this->theme_common_path . 'styles/common/*.scss');
                foreach($scss_common_files as $scss_common_file) {
                    $scss_to_compile .= file_get_contents($scss_common_file);
                }

                $scss_compiler = new ScssCompiler();
                $file_contents .= $scss_compiler->compile($scss_to_compile);

            }

            #minimize
            $file_contents = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $file_contents);
//            $file_contents = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $file_contents);

            file_put_contents($file_destiny, $file_contents);
        }
        return true;
    }

    private function generateNavbar($current_page) {

        $hierarchized_static_pages = $this->hierarchyStaticPages();

        $navbar = '';

        $navbar .= $this->generateStaticPagesTree($hierarchized_static_pages, $current_page);

        if(count($this->public_dynamic_contents) > 0)
        {
            foreach($this->public_dynamic_contents as $model_name => $model)
            {
                $active = '';
                if($model_name == $current_page)
                {
                    $active = ' class="active"';
                }
                $navbar .= "<li{$active}>";
                $navbar .= '<a href="/' . $model_name . '">';
                $navbar .= $model['schema']['nice_name'];
                $navbar .= '</a>';
                $navbar .= '</li>';
            }
        }

//        $contact_current = '';
//        if($current_page == 'contact')
//        {
//            $contact_current = ' class="active"';
//        }
//        $navbar .= '<li' . $contact_current . '><a href="/contact">Contacto</a></li>';

        $this->twig_vars['navbar'] = $navbar;
    }

    private function hierarchyStaticPages($parent = 0)
    {
        $hierarchized_static_pages = Array();
        foreach($this->static_pages as $page) {
            if($page['parent_id'] == $parent) {
                $page['children'] = isset($page['children']) ? $page['children'] : $this->hierarchyStaticPages($page['id']);
                $hierarchized_static_pages[] = $page;
            }
        }
        return $hierarchized_static_pages;
    }

    private function generateStaticPagesTree($static_pages, $current)
    {
        $html = '';
        foreach($static_pages as $page) {
            if(isset($page['seo_name']) && $page['seo_name'] == $current) {
                $active = 'active';
            } else {
                $active = '';
            }
            $html .= "<li class='{$active}'>";
            $html .= "<a href='/{$page['seo_name']}'>{$page['title']}";
            if(isSet($page['children']) && count($page['children']) > 0) {
                $html .= '&nbsp;<i class="fas fa-caret-down"></i>';
            }
            $html .= "</a>";

            if(isSet($page['children']) && count($page['children']) > 0) {
                $html .= '<ul>';
                $html .= $this->generateStaticPagesTree($page['children'],$current);
                $html .= '</ul>';
            }
            $html .= '</li>';
        }
        return $html;
    }

    private function handlePageContents()
    {
        if(isset($this->page_content))
        {
            if(isset($this->action_type) && $this->action_type == 'detail')
            {
                $this->twig_vars['page_content'] = array_shift($this->page_content['items']);
            } else {
                $this->twig_vars['page_content'] = $this->page_content;
            }
        } else
        {

            $this->twig_vars['page_content'] = '';
        }
    }

    private function getLanguages()
    {
        $this->twig_vars['current_language'] = $this->visitor_language;
        $this->twig_vars['languages'] = json_decode('{"cat":{"name":"Catalan","native":"Català"},"aa":{"name":"Afar","native":"Afar"},"ab":{"name":"Abkhazian","native":"Аҧсуа"},"af":{"name":"Afrikaans","native":"Afrikaans"},"ak":{"name":"Akan","native":"Akana"},"am":{"name":"Amharic","native":"አማርኛ"},"an":{"name":"Aragonese","native":"Aragonés"},"ar":{"name":"Arabic","native":"العربية","rtl":1},"as":{"name":"Assamese","native":"অসমীয়া"},"av":{"name":"Avar","native":"Авар"},"ay":{"name":"Aymara","native":"Aymar"},"az":{"name":"Azerbaijani","native":"Azərbaycanca / آذربايجان"},"ba":{"name":"Bashkir","native":"Башҡорт"},"be":{"name":"Belarusian","native":"Беларуская"},"bg":{"name":"Bulgarian","native":"Български"},"bh":{"name":"Bihari","native":"भोजपुरी"},"bi":{"name":"Bislama","native":"Bislama"},"bm":{"name":"Bambara","native":"Bamanankan"},"bn":{"name":"Bengali","native":"বাংলা"},"bo":{"name":"Tibetan","native":"བོད་ཡིག / Bod skad"},"br":{"name":"Breton","native":"Brezhoneg"},"bs":{"name":"Bosnian","native":"Bosanski"},"ca":{"name":"Canadian","native":"Canadian"},"ce":{"name":"Chechen","native":"Нохчийн"},"ch":{"name":"Chamorro","native":"Chamoru"},"co":{"name":"Corsican","native":"Corsu"},"cr":{"name":"Cree","native":"Nehiyaw"},"cs":{"name":"Czech","native":"Česky"},"cu":{"name":"Old Church Slavonic / Old Bulgarian","native":"словѣньскъ / slověnĭskŭ"},"cv":{"name":"Chuvash","native":"Чăваш"},"cy":{"name":"Welsh","native":"Cymraeg"},"da":{"name":"Danish","native":"Dansk"},"de":{"name":"German","native":"Deutsch"},"dv":{"name":"Divehi","native":"ދިވެހިބަސް","rtl":1},"dz":{"name":"Dzongkha","native":"ཇོང་ཁ"},"ee":{"name":"Ewe","native":"Ɛʋɛ"},"el":{"name":"Greek","native":"Ελληνικά"},"en":{"name":"English","native":"English"},"eo":{"name":"Esperanto","native":"Esperanto"},"es":{"name":"Spanish","native":"Español"},"et":{"name":"Estonian","native":"Eesti"},"eu":{"name":"Basque","native":"Euskara"},"fa":{"name":"Persian","native":"فارسی","rtl":1},"ff":{"name":"Peul","native":"Fulfulde"},"fi":{"name":"Finnish","native":"Suomi"},"fj":{"name":"Fijian","native":"Na Vosa Vakaviti"},"fo":{"name":"Faroese","native":"Føroyskt"},"fr":{"name":"French","native":"Français"},"fy":{"name":"West Frisian","native":"Frysk"},"ga":{"name":"Irish","native":"Gaeilge"},"gd":{"name":"Scottish Gaelic","native":"Gàidhlig"},"gl":{"name":"Galician","native":"Galego"},"gn":{"name":"Guarani","native":"Avañe\'ẽ"},"gu":{"name":"Gujarati","native":"ગુજરાતી"},"gv":{"name":"Manx","native":"Gaelg"},"ha":{"name":"Hausa","native":"هَوُسَ","rtl":1},"he":{"name":"Hebrew","native":"עברית","rtl":1},"hi":{"name":"Hindi","native":"हिन्दी"},"ho":{"name":"Hiri Motu","native":"Hiri Motu"},"hr":{"name":"Croatian","native":"Hrvatski"},"ht":{"name":"Haitian","native":"Krèyol ayisyen"},"hu":{"name":"Hungarian","native":"Magyar"},"hy":{"name":"Armenian","native":"Հայերեն"},"hz":{"name":"Herero","native":"Otsiherero"},"ia":{"name":"Interlingua","native":"Interlingua"},"id":{"name":"Indonesian","native":"Bahasa Indonesia"},"ie":{"name":"Interlingue","native":"Interlingue"},"ig":{"name":"Igbo","native":"Igbo"},"ii":{"name":"Sichuan Yi","native":"ꆇꉙ / 四川彝语"},"ik":{"name":"Inupiak","native":"Iñupiak"},"io":{"name":"Ido","native":"Ido"},"is":{"name":"Icelandic","native":"Íslenska"},"it":{"name":"Italian","native":"Italiano"},"iu":{"name":"Inuktitut","native":"ᐃᓄᒃᑎᑐᑦ"},"ja":{"name":"Japanese","native":"日本語"},"jv":{"name":"Javanese","native":"Basa Jawa"},"ka":{"name":"Georgian","native":"ქართული"},"kg":{"name":"Kongo","native":"KiKongo"},"ki":{"name":"Kikuyu","native":"Gĩkũyũ"},"kj":{"name":"Kuanyama","native":"Kuanyama"},"kk":{"name":"Kazakh","native":"Қазақша"},"kl":{"name":"Greenlandic","native":"Kalaallisut"},"km":{"name":"Cambodian","native":"ភាសាខ្មែរ"},"kn":{"name":"Kannada","native":"ಕನ್ನಡ"},"ko":{"name":"Korean","native":"한국어"},"kr":{"name":"Kanuri","native":"Kanuri"},"ks":{"name":"Kashmiri","native":"कश्मीरी / كشميري","rtl":1},"ku":{"name":"Kurdish","native":"Kurdî / كوردی","rtl":1},"kv":{"name":"Komi","native":"Коми"},"kw":{"name":"Cornish","native":"Kernewek"},"ky":{"name":"Kirghiz","native":"Kırgızca / Кыргызча"},"la":{"name":"Latin","native":"Latina"},"lb":{"name":"Luxembourgish","native":"Lëtzebuergesch"},"lg":{"name":"Ganda","native":"Luganda"},"li":{"name":"Limburgian","native":"Limburgs"},"ln":{"name":"Lingala","native":"Lingála"},"lo":{"name":"Laotian","native":"ລາວ / Pha xa lao"},"lt":{"name":"Lithuanian","native":"Lietuvių"},"lv":{"name":"Latvian","native":"Latviešu"},"mg":{"name":"Malagasy","native":"Malagasy"},"mh":{"name":"Marshallese","native":"Kajin Majel / Ebon"},"mi":{"name":"Maori","native":"Māori"},"mk":{"name":"Macedonian","native":"Македонски"},"ml":{"name":"Malayalam","native":"മലയാളം"},"mn":{"name":"Mongolian","native":"Монгол"},"mo":{"name":"Moldovan","native":"Moldovenească"},"mr":{"name":"Marathi","native":"मराठी"},"ms":{"name":"Malay","native":"Bahasa Melayu"},"mt":{"name":"Maltese","native":"bil-Malti"},"my":{"name":"Burmese","native":"Myanmasa"},"na":{"name":"Nauruan","native":"Dorerin Naoero"},"nd":{"name":"North Ndebele","native":"Sindebele"},"ne":{"name":"Nepali","native":"नेपाली"},"ng":{"name":"Ndonga","native":"Oshiwambo"},"nl":{"name":"Dutch","native":"Nederlands"},"nn":{"name":"Norwegian Nynorsk","native":"Norsk (nynorsk)"},"no":{"name":"Norwegian","native":"Norsk (bokmål / riksmål)"},"nr":{"name":"South Ndebele","native":"isiNdebele"},"nv":{"name":"Navajo","native":"Diné bizaad"},"ny":{"name":"Chichewa","native":"Chi-Chewa"},"oc":{"name":"Occitan","native":"Occitan"},"oj":{"name":"Ojibwa","native":"ᐊᓂᔑᓈᐯᒧᐎᓐ / Anishinaabemowin"},"om":{"name":"Oromo","native":"Oromoo"},"or":{"name":"Oriya","native":"ଓଡ଼ିଆ"},"os":{"name":"Ossetian / Ossetic","native":"Иронау"},"pa":{"name":"Panjabi / Punjabi","native":"ਪੰਜਾਬੀ / पंजाबी / پنجابي"},"pi":{"name":"Pali","native":"Pāli / पाऴि"},"pl":{"name":"Polish","native":"Polski"},"ps":{"name":"Pashto","native":"پښتو","rtl":1},"pt":{"name":"Portuguese","native":"Português"},"qu":{"name":"Quechua","native":"Runa Simi"},"rm":{"name":"Raeto Romance","native":"Rumantsch"},"rn":{"name":"Kirundi","native":"Kirundi"},"ro":{"name":"Romanian","native":"Română"},"ru":{"name":"Russian","native":"Русский"},"rw":{"name":"Rwandi","native":"Kinyarwandi"},"sa":{"name":"Sanskrit","native":"संस्कृतम्"},"sc":{"name":"Sardinian","native":"Sardu"},"sd":{"name":"Sindhi","native":"सिनधि"},"se":{"name":"Northern Sami","native":"Sámegiella"},"sg":{"name":"Sango","native":"Sängö"},"sh":{"name":"Serbo-Croatian","native":"Srpskohrvatski / Српскохрватски"},"si":{"name":"Sinhalese","native":"සිංහල"},"sk":{"name":"Slovak","native":"Slovenčina"},"sl":{"name":"Slovenian","native":"Slovenščina"},"sm":{"name":"Samoan","native":"Gagana Samoa"},"sn":{"name":"Shona","native":"chiShona"},"so":{"name":"Somalia","native":"Soomaaliga"},"sq":{"name":"Albanian","native":"Shqip"},"sr":{"name":"Serbian","native":"Српски"},"ss":{"name":"Swati","native":"SiSwati"},"st":{"name":"Southern Sotho","native":"Sesotho"},"su":{"name":"Sundanese","native":"Basa Sunda"},"sv":{"name":"Swedish","native":"Svenska"},"sw":{"name":"Swahili","native":"Kiswahili"},"ta":{"name":"Tamil","native":"தமிழ்"},"te":{"name":"Telugu","native":"తెలుగు"},"tg":{"name":"Tajik","native":"Тоҷикӣ"},"th":{"name":"Thai","native":"ไทย / Phasa Thai"},"ti":{"name":"Tigrinya","native":"ትግርኛ"},"tk":{"name":"Turkmen","native":"Туркмен / تركمن"},"tl":{"name":"Tagalog / Filipino","native":"Tagalog"},"tn":{"name":"Tswana","native":"Setswana"},"to":{"name":"Tonga","native":"Lea Faka-Tonga"},"tr":{"name":"Turkish","native":"Türkçe"},"ts":{"name":"Tsonga","native":"Xitsonga"},"tt":{"name":"Tatar","native":"Tatarça"},"tw":{"name":"Twi","native":"Twi"},"ty":{"name":"Tahitian","native":"Reo Mā`ohi"},"ug":{"name":"Uyghur","native":"Uyƣurqə / ئۇيغۇرچە"},"uk":{"name":"Ukrainian","native":"Українська"},"ur":{"name":"Urdu","native":"اردو","rtl":1},"uz":{"name":"Uzbek","native":"Ўзбек"},"ve":{"name":"Venda","native":"Tshivenḓa"},"vi":{"name":"Vietnamese","native":"Tiếng Việt"},"vo":{"name":"Volapük","native":"Volapük"},"wa":{"name":"Walloon","native":"Walon"},"wo":{"name":"Wolof","native":"Wollof"},"xh":{"name":"Xhosa","native":"isiXhosa"},"yi":{"name":"Yiddish","native":"ייִדיש","rtl":1},"yo":{"name":"Yoruba","native":"Yorùbá"},"za":{"name":"Zhuang","native":"Cuengh / Tôô / 壮语"},"zh":{"name":"Chinese","native":"中文"},"zu":{"name":"Zulu","native":"isiZulu"}}', true);
    }

}