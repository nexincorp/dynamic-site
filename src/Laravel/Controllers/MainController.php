<?php

namespace Osds\DynamicSite\Laravel\Controllers;

use Osds\DynamicSite\Laravel\Traits\ViewTrait;
use Osds\DynamicSite\Traits\UtilsTrait;

class MainController extends BaseController
{

    use UtilsTrait;
    use ViewTrait;

    /* content type of the page (static, dynamic(list, detail) */
    var $content_type = 'static';
    /* content of the page */
    var $page_content = '';
    /* requested uri */
    var $request_uri;

    public function __construct(Request $request)
    {

        parent::__construct($request);

        $this->loadPageContents();

    }

    public function index()
    {
        return $this->generateView();
    }

    private function loadPageContents()
    {
        if(isset($this->request_uri[0]))
        {
            $request = $this->request_uri[0];

            if(in_array($request, array_keys($this->public_dynamic_contents)))
            {
                $this->content_type = 'dyco';
                $this->action_type = 'list';
                $filter = [];
                if(isset($this->request_uri[1]))
                {
                    $this->action_type = 'detail';
                    $filter['get'] = [
                        'search_fields' => [
                            'seo_name' => [
                                'value' => $this->request_uri[1],
                                'operand' => 'LIKE'
                            ]
                        ]
                    ];
                }
                $this->page_content = $this->apiRequest($request, 'list', $filter);
                $this->page_content = $this->preTreatDataBeforeDisplaying($request, $this->page_content, true);


            } else if(in_array($request, array_keys($this->static_pages)))
            {
                $this->page_content = $this->static_pages[$request];
            } else if(strstr($request, 'samples'))
            {
                $this->content_type = 'samples';

            } else if(strstr($request, 'gdpr'))
            {
                $this->content_type = 'gdpr';
            }

            else
            {
                #homepage
                $homepage = $this->config['public']['homepage'];
                $this->request_uri = [$homepage];
                #is set on config file?
                if(isset($this->static_pages[$homepage])) {
                    $this->page_content = $this->static_pages[$homepage];
                } else {
                    # get first result
                    $this->page_content = current($this->static_pages);
                }
            }
        }

    }


}