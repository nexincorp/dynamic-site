<?php

namespace Osds\DynamicSite\Laravel\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Osds\DynamicSite\Laravel\Traits\ViewTrait;
use Osds\DynamicSite\Traits\UtilsTrait;

class FormController extends BaseController
{

    use ViewTrait;
    use UtilsTrait;

    public function __construct(Request $request)
    {

        parent::__construct($request);
    }

    public function display($result = null)
    {
        if(isset($result))
        {
            if(isset($this->config['domain_structure']['language']))
            {
                $user_language = $this->config['domain_structure']['language'];
            } else {
                $lang = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
                $user_language = strtolower(array_shift($lang));
            }

            $literals_file = __DIR__ . '/../../literals/' . $user_language . '.php';
            if(!file_exists($literals_file))
            {
                $literals_file = __DIR__ . '/../../Localization/es-es.php';
            }
            require_once $literals_file;

            Session::put('alert_message', $locale[strtoupper($result)]);
        }

        $this->content_type = 'contact';

        return $this->generateView('pages/contact');
    }

    public function submit()
    {
        $body = '';
        foreach($this->request_data as $param => $value)
        {
            $body .= '\n' . $param . ": " . $value;
        }

        $mail_headers = [];
        if(!empty($this->request_data['visitor email']))
        {
            $headers['From'] = $this->request_data['visitor email'];
        }
        $result = @mail(
            $this->config['public']['contact_email'],
            'Envio de formulario desde su pagina web',
            $body,
            $mail_headers
        );

        if($result)
        {
            $msg = 'SEND_OK';
        } else
        {
            $msg = 'SEND_KO';
        }

        $message = @$this->localization[strtoupper($msg)];
        if($message == null)
        {
            $message = 'Email OK';
        }

        $this->session->put('alert_message', $message);

        $destiny = $_SERVER['HTTP_REFERER'];

        header("Location: {$destiny}");
        exit;
    }


}