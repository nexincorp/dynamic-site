<?php


namespace Osds\DynamicSite\Laravel\Controllers;

use Osds\DynamicSite\Classes\Session;
use Osds\DynamicSite\Commands\BaseCommand;
use Osds\DynamicSite\Laravel\Traits\DynamicContentsTrait;
use Osds\DynamicSite\Laravel\Traits\StaticPagesTrait;
use Osds\DynamicSite\Traits\LocalizationTrait;
use Osds\DynamicSite\Traits\UtilsTrait;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class BaseController extends Controller
{
    use UtilsTrait;
    use StaticPagesTrait;
    use DynamicContentsTrait;
    use LocalizationTrait;

    var $config;

    protected $session;

    protected $request_data;

    protected $content_type;

    protected $visitor_language;

    protected $localization;

    public function __construct(Request $request) {

        $this->session = new Session();

        $this->request_data = $_REQUEST;
        $this->request_uri = explode('/', $request->path());

        $this->loadSiteConfiguration();

        $this->getVisitorLanguage();
        $this->loadCommonLocalization();

        $this->getStaticPages();
        $this->getDynamicContents();


    }

    public function apiRequest($model, $action, $params = [])
    {
        $command = new BaseCommand($model);
        return $command->launch($action, $params);
    }

}