<?php

namespace Osds\Front\UI\Controller;

use Osds\Front\Application\Site\LoadSiteLocalizationApplication;
use Osds\Front\Application\StaticPage\GetStaticPageApplication;
use Osds\Front\Application\StaticPage\GetStaticPageQuery;
use Osds\Front\Application\Dyco\GetDycoListApplication;
use Osds\Front\Application\Dyco\GetDycoListQuery;
use Osds\Front\Domain\Site;
use Osds\Front\Infrastructure\Framework\AppController;
use Osds\Front\Intrastructure\Communication\InputRequest;
use Osds\Front\Intrastructure\Views\ViewInterface;

class GetContentController extends AppController
{

    private $inputRequest;
    private $getStaticPageApplication;
    private $getDycoListApplication;
    private $site;
    private $view;
    private $loadSiteLocalizationApplication;

    public function __construct(
        InputRequest $inputRequest,
        GetStaticPageApplication $getStaticPageApplication,
        GetDycoListApplication $getDycoListApplication,
        LoadSiteLocalizationApplication $loadSiteLocalizationApplication,
        Site $site,
        ViewInterface $view
    )
    {
        $this->inputRequest = $inputRequest;
        $this->getStaticPageApplication = $getStaticPageApplication;
        $this->getDycoListApplication = $getDycoListApplication;
        $this->loadSiteLocalizationApplication = $loadSiteLocalizationApplication;
        $this->site = $site;
        $this->view = $view;
    }

    /**
     *
     * @route GET("/{$entity}")
     *
     */
    public function handle($entity = null)
    {

        #get static pages for listing them in the menu
        $currentStaticPage = null;
        $staticPageQuery = new GetStaticPageQuery('static_pages');
        $staticPagesContent = $this->getStaticPageApplication->execute($staticPageQuery);

        $siteEntities = $this->site->getEntities(true);
        #if we are getting a dyco, request the listing to the db
        if(in_array($entity, array_keys($siteEntities))) {
            $dycoListQuery = new GetDycoListQuery(
                $entity,
                $this->inputRequest->getParameters()
            );
            $dycoListContent = $this->getDycoListApplication->execute($dycoListQuery);
            $this->view->setTemplate('pages/dyco/list');

        } else {
            #display static page
            $dycoListContent = $siteEntities;
            $this->view->setTemplate('pages/static/template');
            if(!is_null($entity)) {
                $currentStaticPage = $entity;
            }
            else {
                # get first result
                $currentStaticPage = current($staticPagesContent);
            }
        }

        $this->loadSiteLocalizationApplication->execute($this->site);

        $this->view->setVariable('currentStaticPage', $currentStaticPage);
        $this->view->setVariable('staticPagesContent', $staticPagesContent);
        $this->view->setVariable('dycoListingContent', $dycoListContent);

        $this->view->render();

    }

}