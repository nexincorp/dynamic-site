<?php

namespace Osds\Front\UI\Controller;

use Osds\Front\Application\Dyco\GetDycoDetailQuery;
use Osds\Front\Domain\Site;
use Osds\Front\Infrastructure\Framework\AppController;

use Osds\Front\Application\Dyco\GetDycoDetailApplication;
use Osds\Front\Intrastructure\Communication\InputRequest;
use Osds\Front\Intrastructure\Views\ViewInterface;

class GetDycoDetailController extends AppController
{

    private $inputRequest;
    private $getDycoDetailApplication;
    private $site;
    private $view;

    public function __construct(
        InputRequest $inputRequest,
        GetDycoDetailApplication $getDycoDetailApplication,
        Site $site,
        ViewInterface $view
    )
    {
        $this->inputRequest = $inputRequest;
        $this->getDycoDetailApplication = $getDycoDetailApplication;
        $this->site = $site;
        $this->view = $view;
    }

    /**
     *
     * @route GET("/{$entity}/{$seoname}")
     *
     */
    public function handle($entity, $seoname)
    {
        $dycoDetailQuery = new GetDycoDetailQuery(
            $entity,
            ['seo_name' => $seoname]
        );
        $dycoDetailContent = $this->getDycoDetailApplication->execute($dycoDetailQuery);

        $this->view->setVariable('entities', $this->site->configuration()->entities(true));
        $this->view->setVariable('dycoDetailContent', $dycoDetailContent);
        $this->view->setTemplate('pages/dyco/detail');

        $this->view->render();

    }

}