<?php

namespace Osds\Front\Application\Common;

abstract class BaseQuery
{

    protected $entity;
    protected $parameters;
    protected $queryStringParameters;
    protected $headers;

    private $uri;
    private $method;

    public function __construct(
        $entity,
        $parameters = [],
        $queryStringParameters = [],
        $headers = []
    )
    {
        $this->entity = $entity;
        $this->parameters = $parameters;
        $this->queryStringParameters = $queryStringParameters;
        $this->headers = $headers;
    }

    protected function setRequestParameters()
    {
        $url = "/{$this->entity}";
        $this->setUri($url);
        $this->setParameters($this->parameters);
        $this->setQueryStringParameters($this->queryStringParameters);
        $this->setHeaders($this->headers);
        $this->setMethod();
    }

    protected function setUri($uri): void
    {
        $this->uri = $uri;
    }

    public function uri(): string
    {
        return $this->uri;
    }

    protected function setParameters($parameters): void
    {
        #if it's not explicitly a post, send it as a get request
        if(!isset($parameters['post'])) {
            $parameters['get'] = $parameters;
        }
        $this->parameters = $parameters;
    }

    public function parameters(): array
    {
        return $this->parameters;
    }

    protected function setQueryStringParameters(array $queryStringParameters)
    {
        if(!isset($this->parameters['get'])) {
            $this->parameters['get'] = $queryStringParameters;
        } else {
            $this->parameters['get'][] = array_merge($this->parameters['get'], $queryStringParameters);
        }
    }

    public function queryStringParameters()
    {

    }

    protected function setHeaders($headers): void
    {
        // TODO: add Beaver header
        $this->headers = $headers;
    }

    public function headers(): array
    {
        return $this->headers();
    }

    protected function setMethod(): void
    {
        if(isset($this->parameters['post'])) {
            $this->method = 'post';
        } else {
            $this->method = 'get';
        }
    }

    public function method(): string
    {
        return $this->method;
    }

}