<?php

namespace Osds\Front\Application\Site;

use Osds\Front\Domain\Site;
use Osds\Front\Infrastructure\Tools;

class LoadSiteLocalizationApplication
{

    public function __construct()
    {
    }

    public function execute(Site $site)
    {
        $visitorLanguage = $site->language();

        $localizationPath = Tools::getFullPath('localization', null);

        $literalsFile = $localizationPath . $visitorLanguage . '.php';
        if(!file_exists($literalsFile))
        {
            $literalsFile = $localizationPath . 'es.php';
        }
        require_once $literalsFile;
    }

}