<?php

namespace Osds\Front\Application\Dyco;

use Osds\Front\Intrastructure\Communication\OutputRequest;

class GetDycoDetailApplication
{

    private $request;

    public function __construct(OutputRequest $request)
    {
        $this->request = $request;
    }

    public function execute(GetDycoDetailQuery $query)
    {
        $this->request->setQuery(
            $query->url(),
            $query->method(),
            [],
            $query->headers()
        );

        $response = $this->request->sendRequest();

        return $response;
    }

}