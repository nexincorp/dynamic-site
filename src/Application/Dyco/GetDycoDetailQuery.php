<?php

namespace Osds\Front\Application\Dyco;

use Osds\Front\Application\Common\BaseQuery;

class GetDycoDetailQuery extends BaseQuery
{

    public function __construct($entity, array $parameters = [], $queryStringParameters = [], array $headers = [])
    {
        parent::__construct($entity, $parameters, $queryStringParameters, $headers);

        $this->setRequestParameters();

    }

}