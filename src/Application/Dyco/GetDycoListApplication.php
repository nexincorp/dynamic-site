<?php

namespace Osds\Front\Application\Dyco;

use Osds\Front\Intrastructure\Communication\OutputRequest;

class GetDycoListApplication
{

    private $request;

    public function __construct(OutputRequest $request)
    {
        $this->request = $request;
    }

    public function execute(GetDycoListQuery $query)
    {
        $this->request->setQuery(
            $query->url(),
            $query->method(),
            $query->parameters(),
            $query->headers(),
        );

        $response = $this->request->sendRequest();

        return $response;
    }

}