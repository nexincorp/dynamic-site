<?php

namespace Osds\Front\Application\StaticPage;

use Osds\Front\Intrastructure\Communication\OutputRequest;

class GetStaticPageApplication
{

    private $request;

    public function __construct(OutputRequest $request)
    {
        $this->request = $request;
    }

    public function execute(GetStaticPageQuery $query)
    {
        $this->request->setQuery(
            $query->uri(),
            $query->method(),
            $query->parameters(),
            $query->headers()
        );

        $response = $this->request->sendRequest();

        return $response;
    }

}