<?php

namespace Osds\Front\Application\StaticPage;

use Osds\Front\Application\Common\BaseQuery;

class GetStaticPageQuery extends BaseQuery
{

    public function __construct($entity, array $parameters = [], $queryStringParameters = [], array $headers = [])
    {
        parent::__construct($entity, $parameters, $queryStringParameters, $headers);

        $this->setRequestParameters();

    }

    protected function setRequestParameters()
    {
        parent::setRequestParameters();
        $this->setUri('/static_page/' . $this->entity);
    }

}