$(document).ready(function() {

    $('#message_terms_and_conds button').on('click', function() {
        document.cookie = "accepted_terms=true";
        $($(this).parent()).remove();
    });

    if(getCookie('accepted_terms') == null) $('#message_terms_and_conds').show();

    $('#social_networks a i').hover(
        function() {
            $(this).addClass($(this).parent('a').attr('title') + '-color');
        },
        function() {
            $(this).removeClass($(this).parent('a').attr('title') + '-color');
        }
    )

});

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}