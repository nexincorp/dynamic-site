<?php

namespace Osds\Front\Domain\ValueObject\Site;

use Osds\Front\Domain\Site;

class Configuration
{

    private $site;
    private $configurations;

    public function __construct(Site $site)
    {
        $this->site = $site;
        $this->setConfigurations();
    }

    private function setConfigurations()
    {
        $configurationPath = $this->site->directories('configuration');
        $configurationFiles = glob($configurationPath);
        foreach($configurationFiles as $file) {
            $fileName = basename($file);
            $this->configurations[$fileName] = file_get_contents($file);
        }

    }

    public function configurations($type = null)
    {
        if($type == null) {
            return $this->configurations;
        } else {
            return $this->configurations[$type];
        }
    }

    public function entities($onlyPublic = false)
    {
        $entities = [];
        foreach($this->configurations['crm']['entities'] as $entityName => $entityData) {
            if(!$onlyPublic ||
                ($onlyPublic && isset($entityData['schema']['public']) && $entityData['schema']['public'] == true)
            ) {
                $entities[$entityName] = $entityData['schema']['nice_name'];
            }
        }
        return $entities;
    }

}