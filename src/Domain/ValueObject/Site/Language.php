<?php

namespace Osds\Front\Domain\ValueObject\Site;

use Osds\Front\Domain\Site;
use Osds\Front\Infrastructure\Persistence\PersistenceInterface;
use Osds\Front\Intrastructure\Communication\InputRequest;

class Language
{

    private $persistedName = 'visitor_language';

    private $language;

    private $request;
    private $persistence;

    public function __construct(InputRequest $request, PersistenceInterface $persistence)
    {
        $this->request = $request;
        $this->persistence = $persistence;

        $this->setLanguage($this->persistence->get($this->persistedName));

        return $this;
    }

    public function obtain(Site $site)
    {
        $requestLang = $this->request->getParameter('lang');
        if(
            empty($lang)
            || !in_array($requestLang, $site->configuration('public')['languages'])
        )
        {
            $requestLang = $site->configuration('public')['default_language'];
        }
        $this->setLanguage($requestLang);
        return $this->get();
    }

    private function setLanguage($language)
    {
        $this->persistence->put($this->persistedName, $requestLang);
        $this->language = $language;
    }

    public function get()
    {
        return $this->language;
    }

}