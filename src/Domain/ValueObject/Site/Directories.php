<?php

namespace Osds\Front\Domain\ValueObject\Site;

use Osds\Front\Domain\Site;
use Osds\Front\Infrastructure\Tools;

class Directories
{

    private $directories;

    public function __construct(Site $site)
    {
        $this->directories['configurationPath'] = Tools::getFullPath('sites_configurations', $site->id());
        $this->directories['publicUri'] = Tools::getFullUri($site->domain(), 'public', $site->id());
    }

    public function get($type = null)
    {
        if(isset($type)) {
            return $this->directories[$type];
        } else {
            return $this->directories;
        }
    }

}