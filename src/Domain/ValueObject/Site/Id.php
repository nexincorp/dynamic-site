<?php

namespace Osds\Front\Domain\ValueObject\Site;

class Id
{

    private $id;

    public function __construct()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $domain = str_replace('www.','', $domain);
        $domain = str_replace('.sandbox','', $domain);
        $this->id = str_replace('.', '_', $domain);
    }

    public function get()
    {
        return $this->id;
    }

}