<?php

namespace Osds\Front\Domain\ValueObject;

class Domain
{

    private $domain;

    public function __construct()
    {
        $this->setDomain();
    }

    private function setDomain()
    {
        $this->domain = $_SERVER['HTTP_HOST'];
    }

    public function domain()
    {
        return $this->domain;
    }

}