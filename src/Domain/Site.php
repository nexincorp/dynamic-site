<?php

namespace Osds\Front\Domain;

use Osds\Front\Domain\ValueObject\Domain;
use Osds\Front\Domain\ValueObject\Site\Configuration;
use Osds\Front\Domain\ValueObject\Site\Directories;
use Osds\Front\Domain\ValueObject\Site\Id;
use Osds\Front\Domain\ValueObject\Site\Language;
use Osds\Front\Infrastructure\Persistence\PersistenceInterface;
use Osds\Front\Intrastructure\Communication\InputRequest;

class Site
{

    private $inputRequest;
    private $persistence;

    private $domain;
    private $id;
    private $directories;
    private $configuration;
    private $language;

    public function __construct(InputRequest $inputRequest, PersistenceInterface $persistence)
    {
        $this->inputRequest = $inputRequest;
        $this->persistence = $persistence;

        $this->domain = new Domain();
        $this->id = new Id();
        $this->directories = new Directories($this);
        $this->configuration = new Configuration($this);
        $this->language = new Language($this->inputRequest, $this->persistence);

        #for roxy file manager
        $_SESSION['site'] = $this;
    }

    /**
     * @return Domain
     */
    public function domain(): Domain
    {
        return $this->domain;
    }

    /**
     * @return Id
     */
    public function id(): Id
    {
        return $this->id;
    }

    /**
     * @param null $type
     * @return array
     */
    public function configuration($type = null): array
    {
        return $this->configuration->configurations($type);
    }

    public function getEntities($onlyPublic = false)
    {
        return $this->configuration->entities($onlyPublic);
    }

    /**
     * @param null $type
     * @return Directories
     */
    public function directories($type = null): Directories
    {
        if(isset($type)) {
            return $this->directories->get($type);
        }
        return $this->directories;
    }

    public function language()
    {
        return $this->language->get();
    }

}