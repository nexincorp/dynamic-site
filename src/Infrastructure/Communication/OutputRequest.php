<?php

namespace Osds\Front\Intrastructure\Communication;

use Config;
use GuzzleHttp\Client as HttpClient;

/**
 * Class used to make HTTP requests
 *
 * Class Request
 * @package Osds\DynamicSite\Classes
 */

class OutputRequest
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var array
     */
    protected $data = array();

    /**
     * @var array
     */
    protected $headers = array();

    /**
     * Constructor.
     *
     * @param string $url The API url
     * @param string $method The HTTP method
     * @param array $data The parameters
     * @param array $headers The HTTP headers
     */
    public function __construct()
    {
    }

    public function setQuery($url = null, $method = null, $data = null, array $headers = array())
    {
        $this->setUrl($url);
        $this->setMethod($method);
        $this->setData($data);
        $this->setHeaders($headers);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }


    public function sendRequest()
    {
        $api_url = '';
        if(strpos(env('API_URL'), 'http') === false)
        {
            $api_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        }
        $api_url .= env('API_URL');

        $client = new HttpClient([
            'base_uri' => $api_url,
            'headers' => [
                'Accept' => 'application/json',
                'X-Auth-Token' => 'PublicTokenForRequestingAPI'
            ]
        ]);

        try {
                if(isset($this->data['post']))
                {
                    $post_data = $this->data['post'];
                } else
                {
                    $post_data = [];
                }

            if(isset($this->data['uri']) && count($this->data['uri']) > 0) {
                $this->url .= '/' . implode('/', $this->data['uri']);
            }
            if(isset($this->data['get']) && count($this->data['get']) > 0) {
                $this->url .= '?' . http_build_query($this->data['get']);
            }

            $response = $client->request(
                $this->method,
                $this->url,
                ['form_params' => $post_data]
            );
            unset($this->data);
        } catch (\Throwable $throwable) {
            throw new \Exception($throwable);
        }
        try {
            $data = json_decode($response->getBody());

            if (is_null($data)) {
                $data = $response->getBody();
            }
        } catch (\Exception $e) {
            $data = $response->getBody();
        }

        return json_decode(json_encode($data), true);

    }

}