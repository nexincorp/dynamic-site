<?php

namespace Osds\Front\Intrastructure\Communication;

use Illuminate\Http\Request as SymfonyRequest;

abstract class InputRequest extends SymfonyRequest
{

    public function getParameter($param)
    {
        return $this->get($param);
    }

    public function getParameters()
    {
        return $this->getAll();
    }

}