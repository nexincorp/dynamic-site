<?php

namespace Osds\Front\Intrastructure\Views;

abstract class AbstractView implements ViewInterface
{

    protected $variables;
    private $template;

    public function setVariables($variables)
    {
        foreach($variables as $key => $value) {
            $this->setVariable($key, $value);
        }
    }

    public function setVariable($key, $value)
    {
        $this->variables[$key] = $value;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function render()
    {
        echo $this->template;
        echo "<pre>";
        var_dump($this->variables);
        exit;
    }

}