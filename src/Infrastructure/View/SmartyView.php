<?php

namespace Osds\Front\Intrastructure\Views;

//use Illuminate\View\View;
//use TwigBridge\Extension\Laravel\Html;

class SmartyView extends AbstractView
{
    private $templateSystem;

    public function __construct(View $smarty)
    {
        $this->templateSystem = $smarty;
    }

    public function setVariable($key, $value)
    {
        $this->templateSystem['twig_vars'][$key] = $value;
    }

    public function render()
    {

    }
}