<?php

namespace Osds\Front\Intrastructure\Views;

interface ViewInterface
{

    public function setVariables($variable);

    public function setVariable($key, $value);

    public function setTemplate($template);

    public function render();

}