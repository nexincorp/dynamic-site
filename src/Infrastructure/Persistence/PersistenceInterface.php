<?php

namespace Osds\Front\Infrastructure\Persistence;

interface PersistenceInterface
{

    public function put($key, $value);

    public function get($key);

    public function remove($key);

}