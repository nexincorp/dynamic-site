<?php

namespace Osds\Front\Infrastructure;

class Tools
{

    public static function getVariables()
    {
        return [
            'relativePaths' => [
                'sites_configurations' => '/site_configurations/%s/',
                'public' => '/public/',
                'styles' => '/public/styles/',
                'localization' => '/vendor/dynamic-site/assets/localization/'
            ]
        ];
    }

    public static function getFullPath($type, $id)
    {
        $classVariables = self::getVariables();

        $basePath = $_SERVER['DOCUMENT_ROOT'];
        $path = $basePath . sprintf($classVariables['relativePaths'][$type], $id);
        return $path;
    }

    public static function getFullUri($baseUri, $type, $id)
    {
        $classVariables = self::getVariables();

        $uri = $baseUri . sprintf($classVariables['relativePaths'][$type], $id);
        return $uri;
    }

}