<?php

$locale['SEND_OK'] = 'El formulario de contacto se ha enviado correctamente, muchas gracias.';
$locale['SEND_KO'] = 'El formulario no se pudo enviar. Por favor, inténtelo de nuevo más tarde.';