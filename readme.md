##Configuración Base del Laravel

* eliminar routes/web.php

* symbolic link de sites_configurations a la carpeta donde esten las configs de los sites
 (en windows cmd, mklink sites_configurations $destiny)

* /.env
    -       API_URL=/api/
    -       API_URL=http://myapidomain.com/api
    -       BACKOFFICE_FOLDER=admin_panel (el que se quiera, si no se utilizará "backoffice" por defecto)

* composer.json
    add "require"
            "osds/api": "*",
            "osds/backoffice": "*",
            "osds/dynamic-site": "*"
            
* composer update

* copiar de /osds/dynamic-site/data/* a / (carpeta public a raiz para tener unos plugins)

* database
- es necesario que /osds/api/src/Framework/Laravel/config/core/config/database.php esté copiado a /config/database.php 
- copiar /osds/api/Framework/Laravel/config/core/database/ a /database/

* crear vhost generico en local si no existe (configs para apache y nginx al final de este documento)

nuevo site
----------
* apuntar en hosts el nombre del site sin tld (samplesite.com -> samplesite.sandbox) a la ip del server local

* create database ($site_id -> samplesite_com) en local

* execute migrations en local
    - ejecutar migrations genericas para un nuevo site en su db correspondiente:
        php artisan migrate --database=$site_id
        
* config:
    - duplicar samplesite_sandbox a $site_id
    
* en este punto $site_id.sandbox deberia ser visible

*editar configuracion            
    - ejecutar migrations customizadas de los sites (si requieren):
        php artisan migrate --database=espiga_sandbox --path="/sites_configurations/espiga_sandbox/user/database/migrations"
    - editar user/config/* a conveniencia



* create tema
    - dentro de /site_configurations/$site_id/user/layout:
        + javascript: js for this theme / site
        + styles:
            · /* css specific for this theme
            · /common/*.scss sass for the common styles (see osds/dynamic-site/src/themes/common/styles/common/*.scss).
                Con esto podemos customizar las variables por defecto de cada uno de los bloques
        
* maquetar paginas en backoffice a partir de los bloques (tinymce, insertar plantilla y customizar texto (y estilo si hace falta) para cada pagina)

* exportar database en local e importar en prod (db $site_id)

* comentarios
crear cuenta para el webmaster en disqus (site_id@nexin.es)
añadir "has_comments" al schema del model
añadir el disqus_id (site_id) al public.yaml del site_configuration

* hacer deploy en prod de sites




* vhost nginx

        server {
            #server_name nexin.es;
            
            listen 80 default server;
            
            access_log  /var/log/nginx/nexin-access.log;
            error_log   /var/log/nginx/nexin-error.log;
        
            root        /var/www/nexin/core/current/public;
            try_files   $uri /$uri /index.php;
            index       index.php index.html index.htm;
        
            charset     utf-8;
        
            error_page 404 /index.php;
        
            location ~ .php$ {
                try_files                       $uri /dev/null =404;
                fastcgi_split_path_info         ^(.+.php)(/.+)$;
                fastcgi_pass                    localhost:9000;
                fastcgi_index                   index.php;
                fastcgi_param                   SCRIPT_FILENAME /var/www/nexin/core/current/public$fastcgi_script_name;
                include                         fastcgi_params;
            }
        }
    
* vhost apache    
        <VirtualHost *:80>
            DocumentRoot "D:/www/nexin/core/public"
            ServerName samplesite.sandbox
            <Directory "D:/www/nexin/core/public">
                AllowOverride All
                Order Allow,Deny
                Allow from all
                Require all granted
            </Directory>
        </VirtualHost>